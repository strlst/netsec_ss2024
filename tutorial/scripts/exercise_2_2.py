import csv

count = {}

with open('../workfiles/mawi_team14.csv') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
    for row in spamreader:
    	if row[5] in count:
    		count[row[5]] += 1
    	else:
    		count[row[5]] = 1


print(count)
