# library imports
import pandas as pd  
from datetime import datetime
from scipy import stats
# Import statistics Library
import statistics
import matplotlib.pyplot as plt
# data imports
dataset1 = pd.read_csv('../workfiles/global_last10years_edit.csv') # loading the data
dataset2 = pd.read_csv('../workfiles/team14_monthly_edit.csv') # loading the data

# calc correlation
pkts_hour2  = [x for x in dataset2.iloc[:,1].tolist() if str(x) != 'nan']
bytes_hour2  = [x for x in dataset2.iloc[:,2].tolist() if str(x) != 'nan']
uIPs_hour2   = [x for x in dataset2.iloc[:,3].tolist() if str(x) != 'nan']
uIPd_hour2  = [x for x in dataset2.iloc[:,4].tolist() if str(x) != 'nan']
time2 = [x for x in dataset2.iloc[:,0].tolist()] #datetime.fromtimestamp(x)

pkts_hour1  = [x for (t, x) in zip(dataset1.iloc[:,0].tolist(), dataset1.iloc[:,2].tolist()) if str(x) != 'nan' and t >= time2[0] and t <= time2[len(time2)-1]]
bytes_hour1  = [x for (t, x) in zip(dataset1.iloc[:,0].tolist(), dataset1.iloc[:,1].tolist()) if str(x) != 'nan' and t >= time2[0] and t <= time2[len(time2)-1]]
uIPs_hour1   = [x for (t, x) in zip(dataset1.iloc[:,0].tolist(),dataset1.iloc[:,3].tolist()) if str(x) != 'nan' and t >= time2[0] and t <= time2[len(time2)-1]]
uIPd_hour1  = [x for (t, x) in zip(dataset1.iloc[:,0].tolist(), dataset1.iloc[:,4].tolist()) if str(x) != 'nan' and t >= time2[0] and t <= time2[len(time2)-1]]
time1 = [x for x in dataset1.iloc[:,0].tolist() if x >= time2[0] and x <= time2[len(time2)-1]]

print("Table A, total sum, mean, median, std.dev")
print("#pkts/hour, %.2f, %.2f, %.2f, %.2f" % (sum(pkts_hour2), sum(pkts_hour2)/len(pkts_hour2), statistics.median(pkts_hour2), statistics.stdev(pkts_hour2)))
print("#bytes/hour, %.2f, %.2f, %.2f, %.2f" % (sum(bytes_hour2), sum(bytes_hour2)/len(bytes_hour2), statistics.median(bytes_hour2), statistics.stdev(bytes_hour2)))
print("#uIPs/hour, %.2f, %.2f, %.2f, %.2f" % (sum(uIPs_hour2), sum(uIPs_hour2)/len(uIPs_hour2), statistics.median(uIPs_hour2), statistics.stdev(uIPs_hour2)))
print("#uIPd/hour, %.2f, %.2f, %.2f, %.2f" % (sum(uIPd_hour2), sum(uIPd_hour2)/len(uIPd_hour2), statistics.median(uIPd_hour2), statistics.stdev(uIPd_hour2)))

print("")

print("Table B, total sum, mean, median, std.dev")
print("#pkts/hour, %.2f, %.2f, %.2f, %.2f" % (sum(pkts_hour1), sum(pkts_hour1)/len(pkts_hour1), statistics.median(pkts_hour1), statistics.stdev(pkts_hour1)))
print("#bytes/hour, %.2f, %.2f, %.2f, %.2f" % (sum(bytes_hour1), sum(bytes_hour1)/len(bytes_hour1), statistics.median(bytes_hour1), statistics.stdev(bytes_hour1)))
print("#uIPs/hour, %.2f, %.2f, %.2f, %.2f" % (sum(uIPs_hour1), sum(uIPs_hour1)/len(uIPs_hour2), statistics.median(uIPs_hour1), statistics.stdev(uIPs_hour1)))
print("#uIPd/hour, %.2f, %.2f, %.2f, %.2f" % (sum(uIPd_hour1), sum(uIPd_hour1)/len(uIPd_hour1), statistics.median(uIPd_hour1), statistics.stdev(uIPd_hour1)))

fig, axs = plt.subplots(2, 2)

axs[0, 0].boxplot([pkts_hour2, pkts_hour1])
axs[0, 0].set_title("#pkts/hour (A, B)")

axs[0, 1].boxplot([bytes_hour2, bytes_hour1])
axs[0, 1].set_title("#bytes/hour (A, B)")

axs[1, 0].boxplot([uIPs_hour2, uIPs_hour1])
axs[1, 0].set_title("#uIPs/hour (A, B)")

axs[1, 1].boxplot([uIPd_hour2, uIPd_hour1])
axs[1, 1].set_title("#uIPd/hour (A, B)")

plt.show()

colors = ['green', 'blue']
labels = ["A", "B"]

fig, axs = plt.subplots(2, 2)

axs[0, 0].hist([pkts_hour2, pkts_hour1], density = True,
         histtype ='bar',
         color = colors,
         label = labels)
axs[0, 0].set_title("#pkts/hour")
axs[0, 1].hist([bytes_hour2, bytes_hour1], density = True,
         histtype ='bar',
         color = colors,
         label = labels)
axs[0, 1].set_title("#bytes/hour")
axs[1, 0].hist([uIPs_hour2, uIPs_hour1], density = True,
         histtype ='bar',
         color = colors,
         label = labels)
axs[1, 0].set_title("#uIPs/hour")
axs[1, 1].hist([uIPd_hour2, uIPd_hour1], density = True,
         histtype ='bar',
         color = colors,
         label = labels)
axs[1, 1].set_title("#uIPd/hour")
 
plt.show()
