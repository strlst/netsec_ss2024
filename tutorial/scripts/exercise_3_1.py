# library imports
import pandas as pd  
import matplotlib.pyplot as plt 
import matplotlib.dates as mdate 
from datetime import datetime
from scipy import stats
# data imports
dataset = pd.read_csv('../workfiles/global_last10years_edit.csv') # loading the data
#create a list with packets_per_day
ts_packets = dataset.iloc[:,1].tolist()
#create a list with the timestamps
raw = dataset['timestamp'] 
#convert each epoch timestamps to the right format
timestamps = [datetime.fromtimestamp(ts) for ts in raw] 
#specify the format of date
date_fmt = '%d-%m-%y %H' 
#use a DateFormatter to set the date to the correct form
date_formatter = mdate.DateFormatter(date_fmt)
fig, ax = plt.subplots(figsize=(10, 5)) #creates 1 figure and subplot
#format the date tick labels of the x-axis
ax.xaxis.set_major_formatter(date_formatter)
ax.xaxis_date() #treats x data as dates
fig.autofmt_xdate() #rotates ticklabels

#set x-axis label
plt.xlabel('days of observed time span') 
plt.ylabel('#bytes/hour [millions]') #sets y-axis label
#sets plot title
plt.title('number of bytes per hour (daily average)')
#plot stem graphic
plt.stem(timestamps, [(x/(10**6)) for x in ts_packets ], markerfmt=" ")
plt.grid()
plt.show()
