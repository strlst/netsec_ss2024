import pandas as pd  
import numpy as np
import pickle as pk
from sklearn.ensemble import RandomForestRegressor

def find_good_hyperparameters(data_training, data_labelled, iterations=8):
        # create random grid map for hyperparameter search
        grid = {'n_estimators': [256, 512, 1024, 2048, 4096],
                'max_depth': [None, 2, 4, 8, 16, 32, 64],
                'min_samples_split': [2, 4, 8],
                'min_samples_leaf': [1, 2, 4],
                'bootstrap': [True, False]}

        # find optimal hyperparameters using a regressor
        empty_regressor = RandomForestRegressor()
        print(f'finding good hyperparameters...', end='')
        # search randomly for parameters
        regressor = RandomizedSearchCV(estimator=empty_regressor, param_distributions=grid, n_iter=iterations, cv=3, verbose=1, random_state=42, n_jobs=-1)
        regressor.fit(data_training, data_labelled)
        params = regressor.best_params_
        print(f'done')

        # return parameters
        print(f'best parameters found are {params}')
        return params

def preprocess_dataset(dataset):
	print(f'preprocessing data...', end='')
	for column in dataset.columns:
		# normalize any array of non-numerical values into numerical categories
		array, index = pd.factorize(dataset[column])
		if index.dtype != 'int64':
			dataset[column] = array,
	print(f'done')
	return dataset