#!/usr/bin/env python3
# library imports
import pandas as pd  
from datetime import datetime
from scipy import stats
import statistics as stats
import matplotlib.pyplot as plt

# data imports
dataset = pd.read_csv('Ex2flows_team14.csv')

counts = dataset.value_counts(['mode(_tcpFlags)'])
#idmax = counts.idxmax()[0]
#print(idmax, counts.max())

total = counts.sum()

# rep-23a
# rep-23c
# rep-23e
print(counts[0 : 3])
for count in counts[0 : 3]:
    # rep-23b
    # rep-23d
    # rep-23f
    print(f'{round(count * 100 / total, 2)}%')

plt.hist(dataset['mode(ipTTL)'], bins=255)
plt.ylabel("frequency")
plt.xlabel("mode(ipTTL)")
plt.show()
#plt.savefig('figures/team14_Ex4_TTLhist.png')
print()