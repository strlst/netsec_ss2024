#!/usr/bin/env python3

import pandas as pd
import statistics as stat 
import numpy as np
import matplotlib.pyplot as plt
import math as m

def interpolate_nans(arr):
    ok = ~np.isnan(arr)
    xp = ok.ravel().nonzero()[0]
    fp = arr[~np.isnan(arr)]
    x  = np.isnan(arr).ravel().nonzero()[0]
    arr[np.isnan(arr)] = np.interp(x, xp, fp)
    return arr

dataset = pd.read_csv('team14_protocol_edit.csv')

# protocol 6 (TCP)

p6_pkts = interpolate_nans(dataset.iloc[:, 1].to_numpy()).tolist()
n = len(p6_pkts) 
#calculate median
median = stat.median(p6_pkts)
#in case of 0 values, replace it with the median
p6_pkts = [ p6_pkts[i] if p6_pkts[i] !=0 else median for i in range(0,n) ]
pkt_fft = np.fft.fft(p6_pkts) #calculates the fft 
pkt_amp = np.abs(pkt_fft) #returns absolute values
k = range(0,n-1) #creates an array from 0 to n-1 
#set k values in the x-axis and specify the limit
x = k[1:m.floor(n/2)] 
#set amp. values in the y-axis and specify the limit
y = pkt_amp[1:m.floor(n/2)] 
#find max index and value
max_k = np.flip(np.argsort(pkt_amp[1:m.floor(n/2)]))[0]
max_amp = pkt_amp[1:m.floor(n/2)][max_k]
print(f'pkts max_k: {max_k + 1}, max_amp: {round(max_amp / 1e6, 2)}')

# period calculation
print(f'period: {n}')

plt.figure(figsize=(10, 5))
plt.plot(x, y, label='Amplitude Spectrum')
plt.xlim(1, m.floor(n/2))
plt.xlabel('k (Frequency index)')
plt.ylabel('Amplitude [millions of pkts]')
plt.title('Amplitude Spectrum for #pkts')
plt.axvline(x=x[max_k], color='r', linestyle='--', label=f'Max Amp at k={x[max_k]}')
plt.legend()
plt.grid(True)
plt.show()

p6_uips = interpolate_nans(dataset.iloc[:, 2].to_numpy()).tolist()
n = len(p6_uips) 
#calculate median
median = stat.median(p6_uips)
#in case of 0 values, replace it with the median
p6_uips = [ p6_uips[i] if p6_uips[i] !=0 else median for i in range(0,n) ]
uips_fft = np.fft.fft(p6_uips) #calculates the fft 
uips_amp = np.abs(uips_fft) #returns absolute values
k = range(0,n-1) #creates an array from 0 to n-1 
#set k values in the x-axis and specify the limit
x = k[1:m.floor(n/2)] 
#set amp. values in the y-axis and specify the limit
y = uips_amp[1:m.floor(n/2)] 
#find max index and value
max_k = np.flip(np.argsort(uips_amp[1:m.floor(n/2)]))[0]
max_amp = uips_amp[1:m.floor(n/2)][max_k]
print(f'uips max_k: {max_k + 1}, max_amp: {round(max_amp / 1e6, 2)}')

# period calculation
print(f'period: {int(n/(max_k + 1))}')

plt.figure(figsize=(10, 5))
plt.plot(x, y, label='Amplitude Spectrum')
plt.xlim(1, m.floor(n/2))
plt.xlabel('k (Frequency index)')
plt.ylabel('Amplitude [millions of pkts]')
plt.title('Amplitude Spectrum for #pkts')
plt.axvline(x=x[max_k], color='r', linestyle='--', label=f'Max Amp at k={x[max_k]}')
plt.legend()
plt.grid(True)
plt.show()