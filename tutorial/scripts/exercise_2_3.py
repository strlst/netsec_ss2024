import csv

num_all = 0
num_eq_1 = 0
num_gt_10 = 0
header = True

with open('../workfiles/Ex2flows_team14.csv') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
    for row in spamreader:
    	if header:
    		header = False
    	else:
	    	num_all += 1
	    	#print(row[2])
	    	if int(row[2]) < 2:
	    		num_eq_1 += 1
		if int(row[2]) > 10:
	    		num_gt_10 += 1
    	
print(num_all)
print(num_eq_1)
print(num_gt_10)
print(float(num_eq_1) / num_all)
print(float(num_gt_10) / num_all)
