# library imports
import pandas as pd  
from datetime import datetime
from scipy import stats
# Import statistics Library
import statistics
# data imports
dataset = pd.read_csv('../workfiles/global_last10years_edit.csv') # loading the data

# calc correlation
pkts_hour  = [x for x in dataset.iloc[:,2].tolist() if str(x) != 'nan']
bytes_hour  = [x for x in dataset.iloc[:,1].tolist() if str(x) != 'nan']
uIPs_hour   = [x for x in dataset.iloc[:,3].tolist() if str(x) != 'nan']
uIPd_hour  = [x for x in dataset.iloc[:,4].tolist() if str(x) != 'nan']
time = [datetime.fromtimestamp(x) for x in dataset.iloc[:,0].tolist()]
print("rep-15a:")
print(stats.pearsonr(pkts_hour, uIPd_hour))
print("rep-15b:")
print(stats.pearsonr(pkts_hour, bytes_hour))
print("rep-15c:")
print(stats.pearsonr(uIPs_hour, uIPd_hour))
print("rep-16:")
print(float(statistics.median(uIPd_hour)) / statistics.median(uIPs_hour))
print("rep-17")
print(time[uIPs_hour.index(max(uIPs_hour))])

