#!/usr/bin/env python3
# library imports
import pandas as pd  
from datetime import datetime
from scipy import stats
# Import statistics Library
import statistics
import matplotlib.pyplot as plt

# data imports
dataset_total = pd.read_csv('team14_monthly_edit.csv')
datasets = pd.read_csv('team14_protocol_edit.csv')

# filter datasets to relevant data
total = dataset_total.iloc[:, lambda df: [0, 1, 3, 4]].dropna()
protocols = [
    datasets.iloc[:, lambda df: [0, 1, 2, 3]].dropna(),
    datasets.iloc[:, lambda df: [0, 4, 5, 6]].dropna(),
    datasets.iloc[:, lambda df: [0, 7, 8, 9]].dropna(),
]

# utility function
avg = lambda series: sum(series) / len(series)

# packet analysis
total_packets = avg(total.iloc[:, 1])
protocol_packets = sum([
    avg(protocols[0].iloc[:, 1]),
    avg(protocols[1].iloc[:, 1]),
    avg(protocols[2].iloc[:, 1]),
])

other_packets = total_packets - protocol_packets
print(f'total packets:    {round(total_packets, 2)}')
print(f'protocol packets: {round(protocol_packets, 2)}')
print(f'other packets:    {round(other_packets, 2)}')
print(f'other/total:      {round(other_packets / total_packets * 100, 2)}')
print()

# ip source analysis
total_source = avg(total.iloc[:, 2])
protocol_source = sum([
    avg(protocols[0].iloc[:, 2]),
    avg(protocols[1].iloc[:, 2]),
    avg(protocols[2].iloc[:, 2]),
])

other_source = total_source - protocol_source
print(f'total source:    {round(total_source, 2)}')
print(f'protocol source: {round(protocol_source, 2)}')
print(f'other source:    {round(other_source, 2)}')
print(f'other/total:     {round(other_source / total_source * 100, 2)}')
print()

# ip destination analysis
total_dest = avg(total.iloc[:, 3])
protocol_dest = sum([
    avg(protocols[0].iloc[:, 3]),
    avg(protocols[1].iloc[:, 3]),
    avg(protocols[2].iloc[:, 3]),
])

other_dest = total_dest - protocol_dest
print(f'total dest:    {round(total_dest, 2)}')
print(f'protocol dest: {round(protocol_dest, 2)}')
print(f'other dest:    {round(other_dest, 2)}')
print(f'other/total:   {round(other_dest / total_dest * 100, 2)}')
print()
