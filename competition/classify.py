#!/usr/bin/env python3

import sys
import pandas as pd
import numpy as np
import pickle as pk
from sklearn.metrics import classification_report, confusion_matrix

# import local package
from util import *

# check for existence of argument
if len(sys.argv) < 3:
    print('usage: classify.py infile.csv outfile.csv')
    exit(1)

# data import
dataset = pd.read_csv(sys.argv[1])

# drop information that is not as relevant
filtered = dataset.drop(columns=['flowStartMilliseconds', 'sourceIPAddress', 'destinationIPAddress'])

# drop newly added columns for this one
dataset = dataset.drop(columns=['protocolIdentifier', 'flowDurationMilliseconds', 'ipTotalLength', 'mode(ipTTL)'])

# normalize/preprocess
#dataset = preprocess_dataset(dataset)

# load classifier
cfile = f'classifier.sav'
print(f'loading trained classifier from {cfile}')
model = pk.load(open(cfile, 'rb'))

# predict
print(f'making model predictions...', end='')
sublabels_prediction = model.predict_proba(filtered)
print('done')

# reduce worst case classifying
# sublabels_prediction[0, :] = sublabels_prediction[0, :] * 0.6

# add prediction to data
sublabel_reverse_mapping = {0: '0', 1: 'Attempted Information Leak', 2: 'Generic Protocol Command Decode', 3: 'Misc activity', 4: 'Misc Attack', 5: 'Potential Corporate Privacy Violation', 6: 'Potentially Bad Traffic'}
sublabels = [sublabel_reverse_mapping[np.argmax(prediction)] for prediction in sublabels_prediction]
#sublabels = []
#for prediction in sublabels_prediction:
    #print(sublabel_reverse_mapping[np.argmax(prediction)], np.argmax(prediction), prediction)
    #sublabels.append(sublabel_reverse_mapping[np.argmax(prediction)])

# report results
#print(f'confusion matrix:')
#print(confusion_matrix(sublabels, sublabels_prediction))
#print(classification_report(sublabels, sublabels_prediction))

# add two annotation columns required for submission
dataset['label'] = ['0' for _ in range(len(sublabels))]
dataset['sublabel'] = sublabels

# finally export annotated dataset
print(f'exporting annotated dataset to {sys.argv[2]}')
dataset.to_csv(sys.argv[2], index=False)
