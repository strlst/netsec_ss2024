#!/usr/bin/env python3

import socket
import struct
import pandas as pd  
from datetime import datetime
from scipy import stats
import statistics as stats
import matplotlib.pyplot as plt

plt.rcParams.update({
    'text.usetex': True,
    'font.family': 'sans-serif',
    'font.size': '24',
    'figure.figsize': [20, 20],
})

# data imports
letters = ['A', 'B', 'C']
files = [f'team14_{letter}.csv' for letter in letters]
datasets = []
for file in files:
    datasets.append(pd.read_csv(file))

source_ip_addresses = [df['sourceIPAddress'] for df in datasets]
dest_ip_addresses = [df['destinationIPAddress'] for df in datasets]

source_ports = [df['sourceTransportPort'] for df in datasets]
dest_ports = [df['destinationTransportPort'] for df in datasets]

legend = []
for source_ips, dest_ips, source_ports, dest_ports, file, letter in zip(source_ip_addresses, dest_ip_addresses, source_ports, dest_ports, files, letters):
    # count top 3 most represented IPs and ports for source
    print(f'{source_ips.value_counts()[:3]}\n')
    print(f'{source_ports.value_counts()[:3]}\n')

    # count top 3 most represented IPs and ports for dest
    print(f'{dest_ips.value_counts()[:3]}\n')
    print(f'{dest_ports.value_counts()[:3]}\n')

    # get source IPs and ports as numbers
    source_numbers = [
        [struct.unpack("!L", socket.inet_aton(ip))[0] for ip in source_ips],
        # use scaling factor to normalize to IP range
        [i * 6.6e4 for i in source_ports],
    ]
    dest_numbers = [
        [struct.unpack("!L", socket.inet_aton(ip))[0] for ip in dest_ips],
        # use scaling factor to normalize to IP range
        [i * 6.6e4 for i in dest_ports],
    ]

    # create and save scatter plot
    plt.clf()
    for source, dest in zip(source_numbers, dest_numbers):
        # add all components separately
        # TODO: ax labels? too complicated
        plt.scatter(source, dest)
    plt.title(file)
    plt.legend(['IPs', 'ports'])
    plot_file = f'figures/team14_Ex4_{letter}_scatter.png'
    print(f'saving plot {plot_file}')
    plt.savefig(plot_file)

    # get dest ip from source
    dest_ip_from_source = [dest_ips.to_list()[i] for i, value in enumerate(source_ips.to_list()) if
                           value == "157.179.193.168"]
    # count reply
    c = 0
    l = []
    for i in dest_ip_from_source:
        if i in source_ips.to_list():
            c += 1
            l.append(i)

    print("Count: ", c)