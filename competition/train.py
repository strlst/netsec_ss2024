#!/usr/bin/env python3

import pandas as pd  
import numpy as np
import pickle as pk
from sklearn.model_selection import train_test_split, RandomizedSearchCV
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score

# import local package
from util import *

# labeled data import
dataset1 = pd.read_csv('training4tuplabeled.csv')
# feature date import
dataset2 = pd.read_csv('training.csv')

# merge data
dataset = pd.merge(dataset1, dataset2, on=['flowStartMilliseconds', 'sourceIPAddress', 'destinationIPAddress', 'sourceTransportPort', 'destinationTransportPort', 'packetTotalCount'])

dataset = dataset.drop(columns=['label'])
sublabel_mapping = {'0': 0, 'Attempted Information Leak': 1, 'Generic Protocol Command Decode': 2, 'Misc activity': 3, 'Misc Attack': 4, 'Potential Corporate Privacy Violation': 5, 'Potentially Bad Traffic': 6}
dataset['sublabel'] = dataset['sublabel'].map(sublabel_mapping)

# extract labeling for later comparison
sublabels = dataset['sublabel']
# drop information that does not exist on the target or is less relevant
dataset = dataset.drop(columns=['sublabel', 'flowStartMilliseconds', 'sourceIPAddress', 'destinationIPAddress'])

# normalize/preprocess
#dataset = preprocess_dataset(dataset)

# split train and test data
print(f'creating train test split...', end='')
dataset_train, dataset_test, sublabels_train, sublabels_test = train_test_split(dataset, sublabels, test_size=0.2, stratify=sublabels)
print(f'done')

# create and fit classifier
#params = find_good_hyperparameters(dataset_train, sublabels_train, 16)
# use previously found parameters
#params = {'n_estimators': 512, 'min_samples_split': 2, 'min_samples_leaf': 4, 'max_depth': 32, 'bootstrap': True}
# use boosted parameters
params = {'n_estimators': 2048, 'min_samples_split': 2, 'min_samples_leaf': 64, 'max_depth': 64, 'bootstrap': True}

classifier = RandomForestClassifier(
        n_estimators=params['n_estimators'],
        max_depth=params['max_depth'],
        min_samples_split=params['min_samples_split'],
        min_samples_leaf=params['min_samples_leaf'],
        bootstrap=params['bootstrap'],
        class_weight='balanced',
        n_jobs=-1,
        verbose=1)
print(f'fitting data...', end='')
classifier.fit(dataset_train, sublabels_train)
print(f'done')

# make predictions
sublabels_prediction_train = classifier.predict(dataset_train)
sublabels_prediction_test = classifier.predict(dataset_test)

# report results
print(f'confusion matrix:')
print(confusion_matrix(sublabels_train, sublabels_prediction_train))
print(classification_report(sublabels_train, sublabels_prediction_train))
print(f'validation:')
print(confusion_matrix(sublabels_test, sublabels_prediction_test))
print(classification_report(sublabels_test, sublabels_prediction_test))

# dump results
out = f'classifier.sav'
print(f'saving trained classifier to {out}')

pk.dump(classifier, open(out, 'wb'))
